Set Serveroutput On

Declare
 USERNAME VARCHAR(20); --:= 'BHAVANA';
Begin
 USERNAME := '&ENTER NAME';
 DBMS_OUTPUT.PUT_LINE(USERNAME || ' WELCOME TO PL/SQL');
End;


--TAKE 2 NUMBER VALUES  FROM USER AND PRINT ADDITION

  Declare
Num1 Number(2):=5;
    Num2 Number(2):=6;    
    result number(4);
Begin
  Num1:=&enter_Num1;
  num2:=&Enter_num2;
  Result:=Num1+Num2;
Dbms_Output.Put_Line('Addition = ' || result);
End;
  
--TAKE EMPNO FROM USER AND PRINT EMP NAME

Declare
 Eno Emp.Empno%Type;  --REFERENCING COLUMN DATA TYPE
 EMPNAME EMP.ENAME%TYPE;
Begin
 Eno:=&enter_Empno;
 Select Ename Into Empname  From Emp Where Empno=Eno;
 DBMS_OUTPUT.PUT_LINE('EMP NAME = ' || EMPNAME);
End;

--TAKE EMPNO FROM USER AND PRINT EMPNAME, DESIG, SAL, HIREDATE, DEPTNO
SELECT COL1, COL2, COL3 INTO VAR1, VAR2, VAR3 FROM TABLE

Declare
 Eno Emp.Empno%Type;
 EMPREC EMP%ROWTYPE;
Begin
 ENO := &ENTER_EMPNO;
 Select * Into Emprec From Emp Where Empno=Eno;
 DBMS_OUTPUT.PUT_LINE(EMPREC.ENAME || CHR(9)||EMPREC.JOB);
End;

--%Type - RefeREnce Column Of A Table  I.E.  Tablename.Colname%Type
--%ROWTYPE - REFERENCE ROW OF A TABLE I.E. TABLENAME%ROWTYPE
Chr(13)
CHAR(13)
Begin
 DBMS_OUTPUT.PUT_LINE('HI ' || CHR(13) || 'HELLO');
End;

/*
If  Condition Then
 .....
END IF;



If  Condition Then
 .....
Else
 .....
END IF;


If  Condition1 Then
 .....
Elsif Condition2 Then
 ...
Elsif Conditionn Then
 ...
Else
 .....
END IF;

*/



Create Table Empcopy AS Select * From Emp;

Select * From Emp;

--TAKE EMPNO FROM USER IF JOB IS PRESIDENT OR MANAGER GIVE 10% INCREMENT 
--FOR ANALYST GIVE 5% INCREMENT
--FOR OTHERS 2% INCREMENT
Declare
 Eno Emp.Empno%Type;
 DESIG EMP.JOB%TYPE;
Begin
 Eno := &enter_Empno;
 Select Job Into Desig From Emp Where Empno=Eno;
 If Desig = 'PRESIDENT' Or Desig='MANAGER' Then
  UPDATE EMP SET SAL = SAL + SAL*.1 WHERE EMPNO=ENO;
 Elsif Desig='ANALYST' Then
  UPDATE EMP SET SAL = SAL + SAL*.05 WHERE EMPNO=ENO;
 Else
  UPDATE EMP SET SAL = SAL + SAL*.02 WHERE EMPNO=ENO;
 END IF; 
END;

Select * From Emp;

Rollback;


WHILE  COND=TRUE
LOOP
  ---
  ---
END LOOP;

LOOP
  ---
  ---
  EXIT CONDITION
END LOOP;

FOR VAR IN START..END LOOP

NEXT;

*/

DECLARE
  VNO NUMBER(3):=1;
BEGIN
  LOOP
    DBMS_OUTPUT.PUT_LINE(VNO);
    VNO:=VNO+1;
    EXIT WHEN VNO >5;
  END LOOP;
  
  Dbms_Output.Put_Line('--------------------------------');
  VNO:=101 ;
  WHILE VNO < 106 LOOP
    DBMS_OUTPUT.PUT_LINE(VNO);
    VNO:=VNO+1;
  END LOOP;
  
  Dbms_Output.Put_Line('--------------------------------');
  FOR NUM IN 201..205  LOOP
    Dbms_Output.Put_Line(Num);    
 END LOOP;
  
  Dbms_Output.Put_Line('--------------------------------');
  
   FOR NUM IN REVERSE 201..205  LOOP
    DBMS_OUTPUT.PUT_LINE(NUM);    
  End Loop;
End;

--PRINT ALL EMP NAME FROM EMP TABLE
Declare
  Empname Emp.Ename%Type;
  CURSOR EMPCURSOR IS SELECT ENAME FROM EMP;
Begin
  If Empcursor%Isopen = False Then
    Open Empcursor ;   
  End If;
  
  Fetch Empcursor Into Empname;
  /*
  Loop
    EXIT WHEN EMPCURSOR%NOTFOUND;
    Dbms_Output.Put_Line(Empname);
    Fetch Empcursor Into Empname;
  End Loop;
  */
  While Empcursor%Found=True 
  Loop
    DBms_Output.Put_Line(Empname);
    Fetch Empcursor Into Empname;
  End Loop;
  DBms_Output.Put_Line('TOTAL ROW PROCESSED = '|| EMPCURSOR%ROWCOUNT);
  CLOSE EMPCURSOR;
END;

Cursor  -- ATTACH QUERY
Open Cursor
Fetch
CLOSE

Cursor Attributes (4)
%Isopen   -- BOOLEAN 
%Found    -- BOOLEAN
%Notfound -- BOOLEAN
%ROWCOUNT -- NUMBER

--TAKE DEPTNO FROM USER AND PRINT EMP NAME



Declare
  Empname Emp.Ename%Type;
  CURSOR EMPCURSOR(DNO NUMBER) IS SELECT ENAME FROM EMP WHERE DEPTNO=DNO;
Begin
  If Empcursor%Isopen = False Then
    Open Empcursor (&ENTER_DEPTNO);   
  End If;
  
  Fetch Empcursor Into Empname;
  
  While Empcursor%Found=True 
  Loop
    DBms_Output.Put_Line(Empname);
    Fetch Empcursor Into Empname;
  End Loop;
  DBms_Output.Put_Line('TOTAL ROW PROCESSED = '|| EMPCURSOR%ROWCOUNT);
  CLOSE EMPCURSOR;
End;


--EXCEPTION

DECLARE
  Emprec Emp%Rowtype;
  ENO NUMBER(2);
BEGIN
  Eno:=&enter_Empno;
  Select * Into Emprec From Emp; --Where Empno=Eno;
  Dbms_Output.Put_Line(Emprec.Ename);
Exception
  When No_Data_Found Then
    Dbms_Output.Put_Line('EMPNO NOT FOUND. PROVIDE VALID EMP NUMBER');
  When Too_Many_Rows Then
    Dbms_Output.Put_Line('PLEASE USE CURSOR FOR MULTIPLE FETCH');
  When Others Then
    Dbms_Output.Put_Line('ERROR');
    Dbms_Output.Put_Line(Sqlcode ||'  '|| Sqlerrm); 
END;



DECLARE
  Emprec Emp%Rowtype;
  ENO NUMBER(4);
BEGIN
  ENO:=&ENTER_EMPNO;
  Select * Into Emprec From Emp Where Empno=Eno;
  DBMS_OUTPUT.PUT_LINE(EMPREC.ENAME);
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DBMS_OUTPUT.PUT_LINE('EMPNO NOT FOUND');
  WHEN TOO_MANY_ROWS THEN
    DBMS_OUTPUT.PUT_LINE('CODE RETURN MORE THEN ONE RECORD - 
        USE CURSOR');
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('ERROR');
     Dbms_Output.Put_Line(Sqlcode ||'  '|| Sqlerrm); 
END;


DECLARE
  TODAY DATE;
BEGIN
  TODAY :=TO_DATE('30-FEB-2017','DD-MON-YYYY');
End;
/

-------
DECLARE
  TODAY DATE;
BEGIN
  TODAY :=TO_DATE('30-FEB-2017','DD-MON-YYYY');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE(SQLCODE ||'  '|| SQLERRM);
END;
/


--PRAGMA EXCEPTION_INIT
DECLARE
  TODAY DATE;
  INVALIDDAY EXCEPTION;
  --PRAGMA EXCEPTION_INIT(USERDEFINENAME, ERRORNO);
  PRAGMA EXCEPTION_INIT(INVALIDDAY,-01839);
BEGIN
  TODAY :=TO_DATE('30-FEB-2017','DD-MON-YYYY');
EXCEPTION
  WHEN INVALIDDAY THEN
    DBMS_OUTPUT.PUT_LINE('DATE FOR THE MONTH IS NOT VALID');
End;
/

--USER DEFINED EXCEPTION --RAISE
DECLARE
  ENO NUMBER(5);
  SALARY EMP.SAL%TYPE;
  LOWSAL EXCEPTION;
BEGIN
  ENO := &ENTER_EMPNO;
  SELECT SAL INTO SALARY FROM EMP WHERE EMPNO=ENO;
  DBMS_OUTPUT.PUT_LINE(SALARY);
  IF SALARY < 3000 THEN
    RAISE LOWSAL;
  END IF;
EXCEPTION
 WHEN LOWSAL THEN
    DBMS_OUTPUT.PUT_LINE('SALARY IS LESS THEN 3000');
End;
/


--RAISE_APPLICATION_ERROR / ERRORNO

DECLARE
  ENO NUMBER(5);
  SALARY EMP.SAL%TYPE; 
BEGIN
  ENO := &ENTER_EMPNO;
  SELECT SAL INTO SALARY FROM EMP WHERE EMPNO=ENO;
  DBMS_OUTPUT.PUT_LINE(SALARY);
  IF SALARY < 3000 THEN
    raise_application_error(-20102,'salary is LOW');
  END IF;
End;
/


--PRAGMA AND RAISE EXPCETION / ERRORNO AND NAME
DECLARE
  ENO NUMBER(5);
  SALARY EMP.SAL%TYPE;
  LOWSAL EXCEPTION;
  PRAGMA EXCEPTION_INIT(LOWSAL,-20102);
BEGIN
  ENO := &ENTER_EMPNO;
  SELECT SAL INTO SALARY FROM EMP WHERE EMPNO=ENO;
  DBMS_OUTPUT.PUT_LINE(SALARY);
  IF SALARY < 3000 THEN
    raise_application_error(-20102,'salary is LOW');
  END IF;
EXCEPTION
 WHEN LOWSAL THEN
  DBMS_OUTPUT.PUT_LINE('SALARY IS LESS THEN 3000');
   Dbms_Output.Put_Line(Sqlcode ||'  '|| Sqlerrm);
End;


--PROCEDURE / STORED PROCEDURE

CREATE OR REPLACE PROCEDURE DEMO IS
Begin
  DBMS_OUTPUT.PUT_LINE('HELLO');
END;


Exec Demo
--TAKE EMPNO AND PRINT ENAME
CrEate Or Replace Procedure Getempname(Eno EMP.EMPNO%TYPE) Is
  EMPNAME EMP.ENAME%TYPE;
Begin
  Select Ename Into Empname From Emp Where Empno=Eno;
  DBMS_OUTPUT.PUT_LINE(EMPNAME);
END;


Exec Getempname(7901)

--DML

CREATE OR REPLACE PROCEDURE INSERTDATATOEMP (ENO EMP.EMPNO%TYPE, EMPNAME EMP.ENAME%TYPE) IS
Begin
  Insert Into Emp(EMPNO,ENAME) Values (Eno, Empname);
  COMMIT;
END;



Exec Insertdatatoemp (1001,'USER1')

--CREATE PROCEDURE WHICH TAKES DEPTNO AS PARAMETER

--DISPLAY EMPNAME, JOB, HIREDATE, SAL, COMM

--IF SAL OF EMP IS NULL HANDLE WITH USER DEFINED EXCEPTION 'MISSING SALARY'

--IF EMP FOR THE DEPTNO NOT FOUND PRINT MESSAGE NO EMP WORKING FOR DEPT


Create Or Replace Procedure Deptwiseempdetail(Dno Emp.Deptno%Type) Is
  Emprec Emp%Rowtype;
  Cursor Empcursor Is Select * From Emp Where Deptno=Dno;
  Salaryexe Exception;
  NOEMPFOUD EXCEPTION;
Begin
  Open EMPCURSOR;
  Fetch EmpcursoR Into Emprec;
  WHILE EMPCURSOR%FOUND = TRUE 
  Loop
    Dbms_Output.Put_Line(Emprec.Ename || ' ' ||Emprec.Sal);
    If Emprec.Sal Is Null Then
      RAISE SALARYEXE;
    END IF;
    FETCH EMPCURSOR INTO EMPREC; 
  End Loop;
  If Empcursor%Rowcount = 0  Then
    RAISE NOEMPFOUD;
  END IF;
  Close Empcursor;
Exception  
  When Salaryexe Then
    Dbms_Output.Put_Line('SALARY IS NULL');
  When NOEMPFOUD Then
    Dbms_Output.Put_Line('NO EMPLOYEE WORKING');
END;

Exec Deptwiseempdetail (100)


Update Emp Set Deptno=10 Where Empno=1001;


Create Or Replace Function Square(Num Number) Return Number Is
  Result Number;
Begin
  Result := Num * Num;
  RETURN RESULT;
END;

Select Square(5) From Dual;

--TAKE EMPNO AND RETURN  NETSAL (SAL + COMM)  OF EMPLOYEE

Create Or Replace Function Empnetsal(Eno Number) Return Number Is
  --Emprec Emp%Rowtype;
  NETSAL NUMBER;
Begin
  Select Nvl(Sal,0) + Nvl(Comm,0)  Into Netsal From Emp Where Empno=Eno;
  Return Netsal;
END;

Select Empnetsal(7901) From DUAL;

Select Empnetsal(1001) From DUAL;

Select Empnetsal(1) From Dual;

Parameter For Procedure 3 Types
In
Out
INOUT

Create Or Replace Procedure Empsalinfo 
  (Eno In Number, Salary Out Number, Commission Out Number, Netsal Out Number) Is
Begin
  Select Sal, Comm, Nvl(Sal,0) + Nvl(Comm,0) Into Salary, Commission, Netsal
    From Emp Where Empno=Eno;
Exception
  When No_Data_Found Then
    Dbms_Output.Put_Line('INVALID EMPNO');
End;

Declare
  NETSAL Number;
  Sal Number;
  COM NUMBER;
Begin
  Empsalinfo(1, Sal, Com, Netsal);
  Dbms_Output.Put_Line('SALARY = ' || Sal || ' COMMISSION = ' 
    || Com || ' NETSAL = '|| Netsal);  
END;